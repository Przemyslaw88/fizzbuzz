﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FizzBuzz.Tests
{
    [TestClass]
    public class FizzBuzzTest
    {
        [TestMethod]
        public void SetDiv1Test()
        {
            //arrange
            FizzBuzz game = new FizzBuzz();

            int div1Value = 3;
            int expected = 3;

            //act
            game.Div1 = div1Value;

            //assert
            Assert.AreEqual(expected, game.Div1, "Incorrect value of div1");
        }

        [TestMethod]
        public void SetDiv2Test()
        {
            //arrange
            FizzBuzz game = new FizzBuzz();

            int div2Value = 3;
            int expected = 3;

            //act
            game.Div2 = div2Value;

            //assert
            Assert.AreEqual(expected, game.Div2, "Incorrect value of div2");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SetDiv1EqualDiv2ExceptionTest()
        {
            //arrange
            FizzBuzz game = new FizzBuzz();
            int div2Value = 3;
            int div1Value = 3;

            //act
            game.Div2 = div2Value;
            game.Div1 = div1Value;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SetDiv2EqualDiv1ExceptionTest()
        {
            //arrange
            FizzBuzz game = new FizzBuzz();
            int div2Value = 5;
            int div1Value = 5;

            //act
            game.Div1 = div1Value;
            game.Div2 = div2Value;
        }

        [TestMethod]
        [ExpectedException(typeof(ArithmeticException))]
        public void SetDiv1AsZeroExceptionTest()
        {
            //arrange
            FizzBuzz game = new FizzBuzz();
            int div1Value = 0;

            //act
            game.Div1 = div1Value;
        }

        [TestMethod]
        [ExpectedException(typeof(ArithmeticException))]
        public void SetDiv2AsZeroExceptionTest()
        {
            //arrange
            FizzBuzz game = new FizzBuzz();
            int div2Value = 0;

            //act
            game.Div2 = div2Value;
        }

        [TestMethod]
        public void IsDivisibleByDiv1TrueTest()
        {
            //arrange
            FizzBuzz game = new FizzBuzz();

            int value = 6;
            game.Div1 = 3;
            bool expected = true;


            //act
            bool result = game.IsDivisableByDiv1(value);

            //assert
            Assert.AreEqual(expected, result, "Incorrect result divide by div1");
        }

        [TestMethod]
        public void IsDivisibleByDiv1FalseTest()
        {
            //arrange
            FizzBuzz game = new FizzBuzz();

            int value = 8;
            game.Div1 = 3;
            bool expected = false;


            //act
            bool result = game.IsDivisableByDiv1(value);

            //assert
            Assert.AreEqual(expected, result, "Incorrect result divide by div1");
        }

        [TestMethod]
        public void IsDivisibleByDiv2TrueTest()
        {
            //arrange
            FizzBuzz game = new FizzBuzz();

            int value = 5;
            game.Div2 = 5;
            bool expected = true;


            //act
            bool result = game.IsDivisableByDiv2(value);

            //assert
            Assert.AreEqual(expected, result, "Incorrect result divide by div1");
        }

        [TestMethod]
        public void IsDivisibleByDiv2FalseTest()
        {
            //arrange
            FizzBuzz game = new FizzBuzz();

            int value = 7;
            game.Div2 = 5;
            bool expected = false;


            //act
            bool result = game.IsDivisableByDiv2(value);

            //assert
            Assert.AreEqual(expected, result, "Incorrect result divide by div1");
        }

        [TestMethod]
        public void IsDivisibleByDiv1AndDiv2TrueTest()
        {
            //arrange
            FizzBuzz game = new FizzBuzz();

            int value = 15;
            game.Div1 = 3;
            game.Div2 = 5;
            bool expected = true;


            //act
            bool result = game.IsDivisableByDiv1AndDiv2(value);

            //assert
            Assert.AreEqual(expected, result, "Incorrect result divide by div1 and div2");
        }

        [TestMethod]
        public void IsDivisibleByDiv1AndDiv2FalseTest()
        {
            //arrange
            FizzBuzz game = new FizzBuzz();

            int value = 9;
            game.Div1 = 3;
            game.Div2 = 5;
            bool expected = false;


            //act
            bool result = game.IsDivisableByDiv1AndDiv2(value);

            //assert
            Assert.AreEqual(expected, result, "Incorrect result divide by div1 and div2");
        }

        [TestMethod]
        public void GetFizzForDivisibleByDiv1Test()
        {
            //arrange
            FizzBuzz game = new FizzBuzz();

            int value = 9;
            game.Div1 = 3;
            string expected = "Fizz";


            //act
            string result = game.GetText(value);

            //assert
            Assert.AreEqual(expected, result, "Incorrect text for divide by div1");
        }

        [TestMethod]
        public void GetBuzzForDivisibleByDiv2Test()
        {
            //arrange
            FizzBuzz game = new FizzBuzz();

            int value = 10;
            game.Div1 = 3;
            game.Div2 = 5;
            string expected = "Buzz";


            //act
            string result = game.GetText(value);

            //assert
            Assert.AreEqual(expected, result, "Incorrect text for divide by div2");
        }

        [TestMethod]
        public void GetFizzBuzzForDivisibleByDiv1AndDiv2Test()
        {
            //arrange
            FizzBuzz game = new FizzBuzz();

            int value = 15;
            game.Div1 = 3;
            game.Div2 = 5;
            string expected = "FizzBuzz";


            //act
            string result = game.GetText(value);

            //assert
            Assert.AreEqual(expected, result, "Incorrect text for divide by div1 and div2");
        }

        [TestMethod]
        public void GetTextForNotDivisibleByDiv1AndDiv2Test()
        {
            //arrange
            FizzBuzz game = new FizzBuzz();

            int value = 17;
            game.Div1 = 3;
            game.Div2 = 5;
            string expected = value.ToString();


            //act
            string result = game.GetText(value);

            //assert
            Assert.AreEqual(expected, result, "Incorrect text for divide by div1 and div2");
        }

        [TestMethod]
        public void RunGameTest()
        {
            //arrange
            FizzBuzz game = new FizzBuzz();

            int value = 15;
            game.Div1 = 3;
            game.Div2 = 4;
            string expected = "1 2 Fizz Buzz 5 Fizz 7 Buzz Fizz 10 11 FizzBuzz 13 14 Fizz ";


            //act
            string result = game.RunGame(value);

            //assert
            Assert.AreEqual(expected, result, "Incorrect text for divide by div1 and div2");
        }
    }
}
