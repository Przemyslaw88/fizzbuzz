﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz
{
    public class FizzBuzz
    {
        #region fields
        private int div1;
        private int div2;
        #endregion

        #region accessors
        public int Div1
        {
            get { return div1; }
            set
            {
                if (value != 0)
                {
                    if (this.div2 != value)
                        this.div1 = value;
                    else
                        throw new System.ArgumentException("Dividers should not be the same");
                }
                else
                {
                    throw new System.ArithmeticException("Can't divide by 0");
                }
            }
        }

        public int Div2
        {
            get { return div2; }
            set
            {
                if (value != 0)
                {
                    if (this.div1 != value)
                        this.div2 = value;
                    else
                        throw new System.ArgumentException("Dividers should not be the same");
                }
                else
                {
                    throw new System.ArithmeticException("Can't divide by 0");
                }
            }
        }
        #endregion

        #region constructors
        public FizzBuzz()
        {
            this.div1 = 29;
            this.div2 = 32;
        }
        #endregion

        #region methods
        public bool IsDivisableByDiv1(int num)
        {
            return (num % this.div1) == 0;
        }

        public bool IsDivisableByDiv2(int num)
        {
            return (num % this.div2) == 0;
        }

        public bool IsDivisableByDiv1AndDiv2(int num)
        {
            return IsDivisableByDiv1(num) && IsDivisableByDiv2(num);
        }

        public string GetText(int num)
        {
            if (IsDivisableByDiv1AndDiv2(num))
                return "FizzBuzz";
            else if (IsDivisableByDiv1(num))
                return "Fizz";
            else if (IsDivisableByDiv2(num))
                return "Buzz";
            else
                return num.ToString();
        }

        public string RunGame(int num)
        {
            StringBuilder strB = new StringBuilder();
            for (int i = 1; i <= num; i++)
            {
                strB.AppendFormat("{0} ", GetText(i));
            }
            return strB.ToString();
        }
        #endregion
    }
}
